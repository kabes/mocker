# Mocker

Configurable HTTP mocker server.
Support some dynamic behaviour thanks to [rhai scripts](https://rhai.rs/) and [Handlebars templates](https://github.com/sunng87/handlebars-rust).

## Run
```bash
./mocker
```

Arguments:
```bash
-v                increase log level
-c <filename>     set custom config file name
```


## Sample configuration file

mocker.toml
```toml

# listen address; one or both must be defined
http_address='127.0.0.1:7070'    # http listen address
https_address='127.0.0.1:7071'   # https listen address

# configure ssl
ssl_key_file="key.pem"    # ssl private key file (required when https enabled)
ssl_cert_file="cert.pem"  # ssl certificate file (required when https enabled)
ssl_ca_certs_file = "ca.crt"
ssl_require_client_cert = true
ssl_client_cn = ["127.0.0.1"]  # also check SAN
#ssl_client_cn = "127.0.0.1"   # can be only one value

server_workers=2          # number of server workers to run
max_payload=1048576       # max size of request payload in bytes

# load some helpers for handlebars templates in rhai
[rhai_helpers]
rhai_mul="rhai_helper_mul.rhai"

[globals]    # some globals data that can be used in templates
test="123"


[[service]]
name="ping"
path="/ping"      # match only /ping path; path is defined by regular expression
methods="get"     # only get requests; default -> any
response="pong"   # response send for client ; 200 is default


# curl -k -v http://127.0.0.1:7070/test2/ -H 'X-aa: aaa'
[[service]]
name="route2"
path="/test2/.*"               # match paths starting with /test2/
methods=["get", "post"]        # handle get and post

  [service.response]
  status=201                   # custom status code; default 200
  handlebars=true              # use handlebars template engine
  file="route2.hb"             # load content from file
  content_type = "text/plain"  # set response content type (exact)
  headers = [                  # set some headers for response
    {handlebars=true, key="x-aa", value="{{ locals.message }}"},  # this been evaluated as X-aa: ...
    {handlebars=false, key="X-bb", value="{{ not eval}}"},        # plain value
    {key="X-cc", value="1234"},                                   # plain value
    "X-bc: test"               # simplifed
  ]

  [service.request]
  #headers = ["X-aa: aaa"]      # require request headers; short version
  headers = [
    {key="X-aa", value="aaa"}   # longer version
  ]

  [service.locals]             # allow define some variables that
  examples = "true"            # are available in teblates under `locals`
  message = "aaa"


# curl -v http://127.0.0.1:7070/json  -H 'content-type: application/json' -XPOST -d '{"value": 123}'
# >> {"status": "ok", "req": "123"}
[[service]]
name="route3"
path="/json$"
methods=["post"]

  [service.response]
  handlebars=true
  text="""{"status": "ok", "req": "{{req_json.value}}"}"""  # use direct text
  status=203
  content_type="application/json"

  [service.request]
  content_type = "application/json*"  #  match also application/json;encoding=utf8


[[service]]
name="route4-file"
path="/file"
response={file="mocker.toml", status=205}   # return static file and custom status


# query remote service (simple proxy)
[[service]]
name="route5-remote"
path="/remote"

  [service.response.remote]
  url="https://localhost:7071/ping"   # rustls required hostname
  method="get"
  copy_status=true            # copy remote response status
  copy_request_headers=true   # copy headers to remote request
  copy_response_headers=true  # copy headers from remote response
  send_request_body=true      # send body to remote
  client_key_file="key.pem"   # ssl private key file for mtls
  client_cert_file="cert.pem" # ssl certificate file for mtls
  trusted_ca_file="ca.crt"    # file with trusted ca certificates
  insecure_skip_verify=true   # skip server certificate verification
  headers = ["X-TEST: test"]


# curl -vk http://127.0.0.1:7070/re/122/dd
[[service]]
name="route5-re"
path='/re/\d+(/[a-z]+)?$'     # match pattern by regex; ie: /re/123/abc or /re/234;
                              # single ' required due escaping
response="ok"                 # response send for client ; 200 is default


# curl -k -v http://127.0.0.1:7070/test6 -XPOST -d 'is_ok  test'
# >> ok
[[service]]
name="route6-body"
path='/test6'
response="ok"
request.body = ["is_ok", "test"]  # accept only request with `is_ok` and test in body
#request.body = '^is_ok$'         # accept only request with `is_ok` in body


# curl -k -v http://127.0.0.1:7070/json2  -XPOST -d '{"test": {"status": "ok"}, "val": 123}' -H 'content-type: application/json'
# >> {"status": "ok"}
[[service]]
name="route7-json"
path="/json2"
methods=["post"]

  [service.response]
  text="""{"status": "ok"}"""
  status=200
  content_type="application/json"

  [service.request]
  content_type = "application/json*"    # required to deserialize json
  #content_type = ["application/json", "application/json;charset=UTF-8"]  # support lists
  json = [                   # match by value in json object (all requred)
    "test.status: ^ok$",     # ie {"test": {"status": "ok"}, "val": 12333}
    "val: 123"]
  #json = 'test.status: ^ok$'           # single value also accepted


[[service]]
name="route8-ssl"
path='/test-ssl$'
response="ok"

  [service.auth]
  https_required = true                    # block http connection to this service
  #peer_cert_sn = ["8636940199261182364"]  # require one of this s/n
  #peer_cert_sn = "8636940199261182364"    # require this one s/n
  #peer_cert_issuer_cn = ["k-local-ca-2"]  # require on of this CA CommonNames
  peer_cert_issuer_cn = "k-local-ca-2"     # require this one CA CommonNames
  peer_cert_subject_cn = ["127.0.0.1"]     # require client common name; also match SAN
  #peer_cert_subject_cn = "127.0.0.1"      # accept also single value


[[service]]
name="route9-access"
path='/test-access$'
response="ok"
auth.remote_address_ip = ["192.168.0.0/24", "127.0.0.1/255.0.0.0", "10.0.0.1"]  # match only this sources
#request.remote_address_ip = "192.168.0.0/24"    # match only this sources
auth.basic_auth = ["user:pass", "user2:pass2"] # required basic authentication
#auth.basic_auth = "user:pass"                # required user:pass as basic authentication


# curl -k -v http://127.0.0.1:7070/path/aaa/123 -H 'X-aa: aaa'
# >> name: aaa; id: 123
[[service]]
name="route10-path-grops"
path='^/path/(?P<name>[a-z]+)/(?P<id>\d+)$'   # parse url and add `name` and `id` to context.path_cap
response.handlebars=true                      # response is template
response.text = '''name: {{ path_cap.name }}; id: {{ path_cap.id }}'''


# curl -k -v http://127.0.0.1:7070/script/sss/123
# >> sss = 123
[[service]]
name="route11-scripts"
path='^/script/(?P<name>[a-z]+)/(?P<id>\d+)$' # parse url and add `name` and `id` to context.path_cap
script="route11.rhai:process"                 # call this file:function and evaluate result
response.handlebars=true                      # response is template
response.text = '''{{ locals.result.combined_data }}'''  # script execution result


# more "realistic" example
# curl -k -v https://127.0.0.1:7071/route12/test -XPOST -H 'content-type: application/json' --data '{"arg1": 123, "arg2": [1, 2, 3]}'
# >> {"arg1x2":246,"arg2_sum":6,"local_test":"abc","orginal_request":{"arg1":123,"arg2":[1,2,3]},"requested_name":"test","test":true,"test_data":[4]}
# curl -k -v https://127.0.0.1:7071/route12/test -XPUT -H 'content-type: application/json' --data '{"arg1": 123, "arg2": [1, 2, 3]}'
# >> "put ok but stop now"
[[service]]
name="route12-generator-json"
path="^/route12/(?P<name>[a-zA-Z0-9]+)$"
script="route12.rhai:process"
response.handlebars=true                      # response is template
response.text = '''{{#json_to_str locals}}{{/json_to_str}}'''
```

### json request and responses
For `content-type: application/json` request body is parsed to json object and available in handlebars context (under `req_json` variable);

Example:
```
...
[[service]]
name="json"
path="/json"
methods=["post"]

  [service.request]
  content_type = "application/json"

  [service.response]
  content_type = "application/json"
  handlebars=true
  text="""{"status": "ok", "req": "{{req_json.value}}"}"""
...

```

Request:
```bash
curl -kv https://127.0.0.1:7070/json -H 'X-aa: aaa' -H 'content-type: application/json' --data '{"value": 123}' -XPOST
```

Result:
```json
{"status": "ok", "req": "123"}
```

## Handlebars context
Data available in handlebars template:

| Name                            | Type                    | Value                                  |
|---------------------------------|-------------------------|----------------------------------------|
| service_name                    | String                  | Name of service                        |
| method                          | String                  | Http method                            |
| path                            | String                  | Request path                           |
| path_cap                        | HashMap<String, String> | Groups captured by regex from req path |
| query_string                    | String                  | Request query as string                |
| query                           | HashMap<String, String> | Request query map                      |
| headers                         | HashMap<String, String> | Request headers                        |
| req_json                        | Option<json>            | Request body as json object            |
| req_body                        | String                  | Request body as string                 |
| globals                         | HashMap<String, json>   | Global variables                       |
| locals                          | HashMap<String, json>   | Local variables                        |
| connection.ssl                  | bool                    | Ssl connection                         |
| connection.peer_cert_sn         | String                  | Client certificate serial number       |
| connection.peer_cert_issuer_cn  | String                  | Client certificate issuer common name  |
| connection.peer_cert_subject_cn | String                  | Client certificate subject common name |
| connection.remote_address_ip    | IpAddr                  | Remote address                         |
| response_headers                | HashMap<String, String> | Additional header set by script        |
| response_status                 | Option<u16>             | Response status overwritten by script  |



## Internal services

| Url         | Function               |
|-------------|------------------------|
| `/_metrics` | prometheus metrics     |
| `/_health`  | return always http 200 |



## Licence
Copyright (c) Karol Będkowski, 2021

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

For details please see COPYING file.
