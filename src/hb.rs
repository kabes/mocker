// Handlebars support
// hb.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//

use handlebars::{Context, Handlebars, Helper, HelperResult, Output, RenderContext, RenderError};
use std::collections::HashMap;
use std::sync::RwLock;

use crate::{errors, json};

lazy_static! {
    static ref HANDLEBARS: RwLock<Handlebars<'static>> = RwLock::new({
        let mut hb = Handlebars::new();
        hb.register_escape_fn(handlebars::no_escape);
        hb.register_helper("json-path", Box::new(json_path_helper));
        handlebars_misc_helpers::register(&mut hb);
        hb
    });
}

pub fn register_rhai_helpers(files: &HashMap<String, String>) -> Result<(), errors::PrepareError> {
    let mut hb = HANDLEBARS.write().unwrap();
    for (name, file) in files.iter() {
        hb.register_script_helper_file(name, file).map_err(|err| {
            errors::PrepareError::RhaiLoadError {
                filename: name.to_owned(),
                error: err.to_string(),
            }
        })?;
    }
    Ok(())
}

pub fn register_text(s: &str) -> Result<(), errors::PrepareError> {
    HANDLEBARS
        .write()
        .unwrap()
        .register_template_string(s, s)
        .map_err(|err| errors::PrepareError::ParseTemplateError {
            template: s.to_owned(),
            error: err.to_string(),
        })?;
    Ok(())
}

pub fn register_file(s: &str) -> Result<(), errors::PrepareError> {
    HANDLEBARS
        .write()
        .unwrap()
        .register_template_file(s, s)
        .map_err(|err| errors::PrepareError::ParseTemplateError {
            template: s.to_owned(),
            error: err.to_string(),
        })?;
    Ok(())
}

pub fn render(
    tmpl: &str,
    ctx: &crate::context::ConnectionContext,
) -> Result<String, errors::ResponseError> {
    let res = {
        let handlebars = HANDLEBARS.read().unwrap();
        handlebars.render(tmpl, ctx)
    };
    if let Err(err) = &res {
        error!("render template `{}` error: {}", tmpl, err.to_string());
    }
    res.map_err(|err| errors::ResponseError::TemplateError {
        template: tmpl.to_string(),
        error: err.to_string(),
    })
}

// implement via bare function
fn json_path_helper(
    h: &Helper,
    _: &Handlebars,
    _: &Context,
    _rc: &mut RenderContext,
    out: &mut dyn Output,
) -> HelperResult {
    let obj = h
        .param(0)
        .ok_or_else(|| RenderError::new("param 1 (object) not found"))?
        .value();
    let path = h
        .param(1)
        .ok_or_else(|| RenderError::new("param 2 (path) not found"))?
        .value()
        .as_str()
        .ok_or_else(|| RenderError::new("param 2 (path) must be string"))?;

    let value = json::json_query_to_str(obj, path);
    let value = value.unwrap_or_default();
    out.write(value.as_ref())?;
    Ok(())
}
