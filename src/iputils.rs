//
// iputils.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//
use crate::errors::PrepareError;
use std::convert::TryFrom;
use std::fmt;
use std::net::{IpAddr, Ipv4Addr};

pub fn create_netmask(bits: u8) -> u32 {
    match bits {
        0 => 0,
        32 => 0xffffffff,
        _ => (((1u32 << bits) - 1) << (32 - bits)) as u32,
    }
}

#[derive(Clone)]
pub enum IpMatcher {
    Ip(Ipv4Addr),
    Network((u32, u32)),
}

impl fmt::Debug for IpMatcher {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            IpMatcher::Ip(addr) => f.write_fmt(format_args!("Ip: {:?}", addr)),
            IpMatcher::Network((net, mask)) => f.write_fmt(format_args!(
                "Network: {}.{}.{}.{}/{}.{}.{}.{}",
                (net & 0xFF000000) >> 24,
                (net & 0x00FF0000) >> 16,
                (net & 0x0000FF00) >> 8,
                (net & 0x000000FF),
                (mask & 0xFF000000) >> 24,
                (mask & 0x00FF0000) >> 16,
                (mask & 0x0000FF00) >> 8,
                (mask & 0x000000FF),
            )),
        }
    }
}

fn parse_netmask(netmask: &str) -> Result<u32, PrepareError> {
    if let Ok(bits) = netmask.parse::<u8>() {
        // netmask as num
        Ok(create_netmask(bits))
    } else {
        match netmask.parse::<IpAddr>() {
            Ok(IpAddr::V4(netmask)) => {
                let mask: u32 = netmask.into();
                Ok(mask)
            }
            _ => Err(PrepareError::GenericError(format!(
                "ipv6 for {} not supported",
                netmask
            ))),
        }
    }
}

// TODO: ipv6
impl TryFrom<&str> for IpMatcher {
    type Error = PrepareError;

    fn try_from(s: &str) -> Result<Self, Self::Error> {
        if let Some((ipstr, netmask)) = s.split_once('/') {
            let ip: IpAddr = ipstr.parse().map_err(|_| {
                PrepareError::GenericError(format!("failed parse {} to ip address", ipstr))
            })?;

            if let IpAddr::V4(ip) = ip {
                let nip: u32 = ip.into();
                let mask = parse_netmask(netmask)?;
                Ok(IpMatcher::Network((nip & mask, mask)))
            } else {
                return Err(PrepareError::GenericError(format!(
                    "ipv6 for {} not supported",
                    ipstr
                )));
            }
        } else {
            let ip: IpAddr = s.parse().map_err(|_| {
                PrepareError::GenericError(format!("failed parse {} to ip address", s))
            })?;

            if let IpAddr::V4(ip4) = ip {
                Ok(IpMatcher::Ip(ip4))
            } else {
                Err(PrepareError::GenericError(format!(
                    "ipv6 for {} not supported",
                    s
                )))
            }
        }
    }
}

impl IpMatcher {
    pub fn is_match(&self, ip: IpAddr) -> bool {
        match ip {
            IpAddr::V4(ip) => match self {
                IpMatcher::Ip(addr) => *addr == ip,
                IpMatcher::Network((net, mask)) => {
                    let ip: u32 = ip.into();
                    (*net) == ip & mask
                }
            },
            IpAddr::V6(_) => false, // not supported
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_netmask() {
        assert_eq!(create_netmask(0), 0x00000000);
        assert_eq!(create_netmask(8), 0xff000000);
        assert_eq!(create_netmask(16), 0xffff0000);
        assert_eq!(create_netmask(24), 0xffffff00);
        assert_eq!(create_netmask(32), 0xffffffff);
        assert_eq!(create_netmask(9), 0xff800000);
    }
}
