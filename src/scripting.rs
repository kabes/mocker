//
// scripting.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//
use crate::context::ConnectionContext;
use rhai::{Engine, EvalAltResult, Scope, AST};
use std::collections::HashMap;
use std::fmt;
use std::str::FromStr;
use thiserror::Error;

const MAX_EXPR_DEPTH: usize = 32;
const MAX_FUNCTION_EXPR_DEPTH: usize = 32;

#[allow(clippy::enum_variant_names)]
#[derive(Error, Debug, Clone)]
pub enum ScriptError {
    /// any error
    #[error("{0}")]
    GenericError(String),

    /// compile script error
    #[error("compile file {filename} error {error}")]
    CompileError { filename: String, error: String },

    /// user generated error
    #[error("execute function {function} from {filename} [{position}] result {result}")]
    UserError {
        filename: String,
        function: String,
        position: String,
        result: ScriptResult,
    },

    /// non-user error
    #[error("execute function {function} from {filename} [{position}] internal error {error}")]
    InternalError {
        filename: String,
        function: String,
        position: String,
        error: String,
    },
}

/// ScriptResult may be created in scripts and stop future processing.
#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct ScriptResult {
    /// http status for response
    pub status: u16,
    /// response body
    pub result: serde_json::Value,
    /// response headers
    pub headers: HashMap<String, String>,
}

impl ScriptResult {
    pub fn new_error(status: i64, message: &str) -> Self {
        ScriptResult {
            status: status as u16,
            result: serde_json::Value::String(message.to_string()),
            headers: HashMap::new(),
        }
    }

    pub fn new(status: i64) -> Self {
        ScriptResult {
            status: status as u16,
            result: serde_json::Value::Null,
            headers: HashMap::new(),
        }
    }

    pub fn set_result(&mut self, result: rhai::Dynamic) {
        self.result = rhai::serde::from_dynamic::<serde_json::Value>(&result).unwrap();
    }

    pub fn set_header(&mut self, name: &str, value: &str) {
        self.headers.insert(name.to_string(), value.to_string());
    }
}

impl fmt::Display for ScriptResult {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "status: {:?} message: {}", self.status, self.result)
    }
}

#[derive(Debug, Clone)]
pub struct Script {
    ast: AST,
    function: String,
    filename: String,
}

impl FromStr for Script {
    type Err = ScriptError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (filename, function) = s.split_once(':').ok_or_else(|| {
            ScriptError::GenericError(format!(
                "script must be in format filename:function; given `{}`",
                s
            ))
        })?;

        let mut engine = Engine::new();
        engine.set_max_expr_depths(MAX_EXPR_DEPTH, MAX_FUNCTION_EXPR_DEPTH);
        let ast =
            engine
                .compile_file(filename.into())
                .map_err(|err| ScriptError::CompileError {
                    filename: filename.to_string(),
                    error: err.to_string(),
                })?;

        Ok(Script {
            ast,
            function: function.to_string(),
            filename: filename.to_string(),
        })
    }
}

impl Script {
    fn new_user_error(
        &self,
        result: ScriptResult,
        position: Option<rhai::Position>,
    ) -> ScriptError {
        ScriptError::UserError {
            filename: self.filename.clone(),
            function: self.function.clone(),
            position: position.map(|p| p.to_string()).unwrap_or_default(),
            result,
        }
    }

    fn new_internal_error(&self, error: &str, position: Option<rhai::Position>) -> ScriptError {
        ScriptError::InternalError {
            filename: self.filename.clone(),
            function: self.function.clone(),
            position: position.map(|p| p.to_string()).unwrap_or_default(),
            error: error.to_string(),
        }
    }

    /// Evaluate `func` from rhai script `ast` and with `ctx` context.
    /// Return value returned by script function or uil response with error.
    pub fn run(&self, ctx: ConnectionContext) -> Result<ConnectionContext, ScriptError> {
        let mut eng = Engine::new();
        eng.set_max_expr_depths(MAX_EXPR_DEPTH, MAX_FUNCTION_EXPR_DEPTH);
        eng.register_type::<ScriptResult>()
            .register_fn("new_result", ScriptResult::new)
            .register_fn("new_error", ScriptResult::new_error)
            .register_fn("set_result", ScriptResult::set_result)
            .register_fn("set_header", ScriptResult::set_header);

        let mut scope = Scope::new();
        let script_ctx = rhai::serde::to_dynamic(&ctx).unwrap();

        let result: Result<rhai::Dynamic, _> =
            eng.call_fn(&mut scope, &self.ast, &self.function, (script_ctx,));
        trace!("evaluate function: {} result: {:?}", self.function, &result);

        match result {
            Ok(value) => {
                if value.is::<()>() {
                    // when no result is returned by function return unmodified ctx
                    Ok(ctx)
                } else if value.is::<ScriptResult>() {
                    let se = value.cast::<ScriptResult>();
                    Err(self.new_user_error(se, None))
                } else {
                    let jv = rhai::serde::from_dynamic::<serde_json::Value>(&value)
                        .map_err(|err| self.new_internal_error(&err.to_string(), None))?;
                    serde_json::from_value::<ConnectionContext>(jv)
                        .map_err(|err| self.new_internal_error(&err.to_string(), None))
                }
            }
            Err(box EvalAltResult::ErrorInFunctionCall(_, _, value, position)) => {
                if let box EvalAltResult::ErrorRuntime(value, _) = value {
                    Err(self.create_script_error(value, position))
                } else {
                    Err(self.new_internal_error(&value.to_string(), Some(position)))
                }
            }
            Err(box EvalAltResult::ErrorRuntime(value, position)) => {
                Err(self.create_script_error(value, position))
            }
            Err(err) => Err(self.new_internal_error(&err.to_string(), None)),
        }
    }

    fn create_script_error(&self, value: rhai::Dynamic, position: rhai::Position) -> ScriptError {
        if value.is::<ScriptResult>() {
            let se = value.cast::<ScriptResult>();
            self.new_user_error(se, Some(position))
        } else {
            self.new_internal_error(&value.to_string(), Some(position))
        }
    }
}
