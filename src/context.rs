//
// context.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//

use actix_web::{web, HttpRequest};
use std::collections::HashMap;
use std::net::{IpAddr, Ipv4Addr};

#[derive(Debug, Clone, Serialize, PartialEq, Deserialize)]
pub struct ConnectionInfo {
    pub ssl: bool,
    pub peer_cert_sn: String,
    pub peer_cert_issuer_cn: String,
    pub peer_cert_subject_cn: String,
    pub peer_cert_subject_san: Option<Vec<String>>,

    pub remote_address_ip: IpAddr,
}

impl Default for ConnectionInfo {
    fn default() -> Self {
        ConnectionInfo {
            ssl: false,
            peer_cert_sn: "".to_string(),
            peer_cert_issuer_cn: "".to_string(),
            peer_cert_subject_cn: "".to_string(),
            peer_cert_subject_san: None,
            remote_address_ip: IpAddr::V4(Ipv4Addr::UNSPECIFIED),
        }
    }
}

/// Context object for handlebars templates
#[derive(Debug, Serialize, Clone, PartialEq, Deserialize, Default)]
pub struct ConnectionContext {
    pub service_name: Option<String>,

    pub method: String,

    /// Request path
    pub path: String,

    /// Request query as string
    pub query_string: String,

    /// Request query map
    pub query: HashMap<String, String>,

    /// Request headers
    pub headers: HashMap<String, String>,

    /// Request body as json object
    pub req_json: Option<serde_json::Value>,

    /// Request body as string
    pub req_body: String,

    /// Global variables
    pub globals: HashMap<String, serde_json::Value>,

    /// local service variables
    pub locals: HashMap<String, serde_json::Value>,

    /// path captures
    pub path_cap: HashMap<String, String>,

    pub connection: ConnectionInfo,

    pub response_headers: HashMap<String, String>,

    pub response_status: Option<u16>,
}

impl ConnectionContext {
    pub fn new(
        req: &HttpRequest,
        body: web::BytesMut,
        globals: HashMap<String, serde_json::Value>,
        conn_info: ConnectionInfo,
    ) -> ConnectionContext {
        let headers = req
            .headers()
            .iter()
            .map(|(k, v)| {
                (
                    k.as_str().to_owned(),
                    v.to_str().unwrap_or_default().to_owned(),
                )
            })
            .collect::<HashMap<String, String>>();

        // parse body as json when content-type is application/json
        let req_json = if body.is_empty() || !check_content_type(req, "application/json") {
            None
        } else {
            match serde_json::from_slice(&body) {
                Ok(jv) => {
                    debug!("parsed body: {:?}", &jv);
                    Some(jv)
                }
                Err(err) => {
                    info!("parse body {:?} error {}", &body, err);
                    None
                }
            }
        };

        let query = if req.query_string().is_empty() {
            HashMap::new()
        } else {
            match actix_web::web::Query::<HashMap<String, String>>::from_query(req.query_string()) {
                Ok(q) => q.into_inner(),
                Err(err) => {
                    warn!("parse query `{}` error: {}", &req.query_string(), err);
                    HashMap::new()
                }
            }
        };

        let req_body = String::from_utf8(body.as_ref().to_vec()).unwrap_or_else(|_| "".to_string());

        ConnectionContext {
            service_name: None,
            method: req.method().to_string(),
            path: req.path().to_owned(),
            query_string: req.query_string().to_owned(),
            headers,
            req_json,
            req_body,
            query,
            globals,
            connection: conn_info,
            ..Default::default()
        }
    }
}

fn check_content_type(req: &HttpRequest, expected: &str) -> bool {
    req.headers()
        .get("content-type")
        .map(|h| h.to_str())
        .map(|hv| hv.unwrap().starts_with(expected))
        .unwrap_or_default()
}
