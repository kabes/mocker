//
// conf.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
use core::slice::Iter;
use serde::de;
use serde::Deserialize;
use std::collections::{HashMap, HashSet};
use std::fmt;
use std::fs::File;
use std::io::prelude::*;
use std::str::FromStr;
use thiserror::Error;

/// Represent configuration object that contains key, value and handlebars flag
#[derive(Debug, Clone, Deserialize, PartialEq, Default, Serialize)]
pub struct TemplatableKeyValue {
    #[serde(default)]
    pub handlebars: bool,
    pub key: String,
    pub value: String,
}

impl FromStr for TemplatableKeyValue {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.split_once(':')
            .map(|(k, v)| TemplatableKeyValue {
                handlebars: false,
                key: k.to_owned(),
                value: v.to_owned(),
            })
            .ok_or_else(|| format!("parse `{}` as key:value failed", s))
    }
}

impl fmt::Display for TemplatableKeyValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        if self.handlebars {
            write!(f, "{}: {} with handlebars", &self.key, &self.value)
        } else {
            write!(f, "{}: {}", &self.key, &self.value)
        }
    }
}

pub trait ValueInnerType = FromStr<Err = String>;

/// StringOrObject contains obejct that can be either string or whole object in config file
#[derive(Debug, Clone, Serialize, PartialEq, Default)]
pub struct StringOrObject<T: ValueInnerType>(T);

impl<T: ValueInnerType> StringOrObject<T> {
    #[allow(dead_code)]
    pub fn new(inner: T) -> Self {
        StringOrObject(inner)
    }

    pub fn inner(&self) -> &T {
        &self.0
    }
}

impl<'de, T: 'de> de::Deserialize<'de> for StringOrObject<T>
where
    T: ValueInnerType + de::Deserialize<'de>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: de::Deserializer<'de>,
    {
        use std::marker::PhantomData;
        struct TomlDependencyVisitor<'de, T>(&'de PhantomData<T>)
        where
            T: ValueInnerType + de::Deserialize<'de>;

        impl<'de, Y> de::Visitor<'de> for TomlDependencyVisitor<'de, Y>
        where
            Y: ValueInnerType + de::Deserialize<'de>,
        {
            type Value = StringOrObject<Y>;

            fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
                formatter.write_str("...")
            }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                Y::from_str(s)
                    .map(StringOrObject)
                    .map_err(de::Error::custom)
            }

            fn visit_map<V>(self, map: V) -> Result<Self::Value, V::Error>
            where
                V: de::MapAccess<'de>,
            {
                let mvd = de::value::MapAccessDeserializer::new(map);
                Y::deserialize(mvd).map(StringOrObject)
            }
        }

        deserializer.deserialize_any(TomlDependencyVisitor(&PhantomData))
    }
}

/// Represent list of value that can be defined also as single value in config.
/// ie key="123", key=["23", "34"]
#[derive(Debug, Clone, Serialize, PartialEq)]
pub struct StringOrList(Vec<String>);

impl StringOrList {
    pub fn iter(&self) -> Iter<String> {
        self.0.iter()
    }

    pub fn inner(&self) -> Vec<String> {
        self.0.clone()
    }
}

impl<'de> de::Deserialize<'de> for StringOrList {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: de::Deserializer<'de>,
    {
        struct TomlDependencyVisitor;

        impl<'de> de::Visitor<'de> for TomlDependencyVisitor {
            type Value = StringOrList;

            fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
                formatter.write_str("...")
            }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                Ok(StringOrList(vec![s.to_owned()]))
            }

            fn visit_seq<V>(self, list: V) -> Result<Self::Value, V::Error>
            where
                V: de::SeqAccess<'de>,
            {
                let mvd = de::value::SeqAccessDeserializer::new(list);
                Deserialize::deserialize(mvd).map(StringOrList)
            }
        }

        deserializer.deserialize_any(TomlDependencyVisitor)
    }
}

/// Configuration for remote services
#[derive(Debug, Deserialize, Clone, Serialize, PartialEq)]
pub struct RemoteResponse {
    /// remote service url; rustls require hostname
    pub url: String,

    /// Additional headers to add to request
    pub headers: Option<Vec<String>>,

    /// Copy http status that return remote service
    #[serde(default)]
    pub copy_status: bool,

    /// Copy origin request headers to remote service request
    #[serde(default)]
    pub copy_request_headers: bool,

    /// Copy headers from remote service response
    #[serde(default)]
    pub copy_response_headers: bool,

    /// Http method for remote request
    #[serde(default = "RemoteResponse::default_method")]
    pub method: String,

    /// Send origin request body to remote service; default true
    #[serde(default = "RemoteResponse::default_send_request_body")]
    pub send_request_body: bool,

    /// Client certificate file for mtls connection to remote
    pub client_cert_file: Option<String>,
    /// Client private key file for mtls connection to remote
    pub client_key_file: Option<String>,

    /// additional trusted ca certificate
    pub trusted_ca_file: Option<String>,

    /// skip server verification
    pub insecure_skip_verify: bool,

    /// scrtip to run to modify remote response
    pub script: Option<String>,
}

impl RemoteResponse {
    fn default_method() -> String {
        "GET".to_string()
    }

    fn default_send_request_body() -> bool {
        true
    }

    fn validate(&self) -> Result<(), ConfigError> {
        if self.url.is_empty() {
            return Err(ConfigError::ValidateError(
                "url must be non-empty".to_string(),
            ));
        }

        {
            let ccf = self.client_cert_file.is_some();
            let ckf = self.client_key_file.is_some();
            if (ccf || ckf) && (ccf != ckf) {
                return Err(ConfigError::ValidateError(
                    "for mtls both certificate and key file must be defined".to_string(),
                ));
            }
        }

        Ok(())
    }
}

pub type Header = StringOrObject<TemplatableKeyValue>;

#[derive(Debug, Deserialize, Clone, Serialize, Default)]
pub struct DetailedResponse {
    /// text respone
    pub text: Option<String>,
    /// response for file
    pub file: Option<String>,
    /// remote from other http service
    pub remote: Option<RemoteResponse>,

    /// use handlebars engine
    #[serde(default)]
    pub handlebars: bool,

    /// response status
    #[serde(default = "DetailedResponse::default_status")]
    pub status: u16,

    /// response content type
    pub content_type: Option<String>,

    /// response headers
    pub headers: Option<Vec<Header>>,
}

impl DetailedResponse {
    fn default_status() -> u16 {
        200
    }

    fn validate(&self) -> Result<(), ConfigError> {
        let num_args: usize = self.text.as_ref().map(|_| 1).unwrap_or(0)
            + self.file.as_ref().map(|_| 1).unwrap_or(0)
            + self.remote.as_ref().map(|_| 1).unwrap_or(0);
        match num_args {
            0 => {
                return Err(ConfigError::ValidateError(
                    "no text/file/remote defined".to_string(),
                ))
            }
            1 => (),
            _ => {
                return Err(ConfigError::ValidateError(
                    "there can be only one argument defined: text/file/remote".to_string(),
                ))
            }
        }

        if let Some(remote) = &self.remote {
            remote.validate()?;
        }

        Ok(())
    }
}

#[derive(Debug, Clone, Serialize)]
#[serde(untagged)]
pub enum Response {
    SimpleResponse(String),
    DetailedResponse(Box<DetailedResponse>),
}

impl Response {
    fn validate(&self) -> Result<(), ConfigError> {
        match self {
            Response::SimpleResponse(_) => Ok(()),
            Response::DetailedResponse(dr) => dr.validate(),
        }
    }
}

impl<'de> de::Deserialize<'de> for Response {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: de::Deserializer<'de>,
    {
        struct TomlDependencyVisitor;

        impl<'de> de::Visitor<'de> for TomlDependencyVisitor {
            type Value = Response;

            fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
                formatter.write_str("...")
            }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                Ok(Response::SimpleResponse(s.to_owned()))
            }

            fn visit_map<V>(self, map: V) -> Result<Self::Value, V::Error>
            where
                V: de::MapAccess<'de>,
            {
                let mvd = de::value::MapAccessDeserializer::new(map);
                DetailedResponse::deserialize(mvd)
                    .map(|r| (Response::DetailedResponse(Box::new(r))))
            }
        }

        deserializer.deserialize_any(TomlDependencyVisitor)
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct Config {
    /// listen address
    pub http_address: Option<String>,

    /// set https address, turn on https
    pub https_address: Option<String>,

    /// ssl key in pem format file name
    pub ssl_key_file: Option<String>,
    /// ssl certificate in pem format file name
    pub ssl_cert_file: Option<String>,

    /// File names with CA certificates list
    pub ssl_ca_certs_file: Option<String>,

    /// turn on client ssl authentication
    pub ssl_require_client_cert: bool,

    /// List of accepted client certificates (cn names)
    pub ssl_client_cn: Option<StringOrList>,

    /// Max size of request payload in bytes
    #[serde(default = "Config::default_max_payload")]
    pub max_payload: usize,

    /// service workers to start
    #[serde(default)]
    pub server_workers: usize,

    /// Global variables
    pub globals: Option<HashMap<String, serde_json::Value>>,

    /// List of rhai files with helpers to load for handlebars templates
    #[serde(default)]
    pub rhai_helpers: Option<HashMap<String, String>>,

    /// services definitions
    #[serde(rename = "service", default)]
    pub services: Vec<Service>,
}

impl Config {
    pub fn http_enabled(&self) -> bool {
        self.http_address
            .as_ref()
            .map(|v| !v.is_empty())
            .unwrap_or(false)
    }

    pub fn https_enabled(&self) -> bool {
        self.https_address
            .as_ref()
            .map(|v| !v.is_empty())
            .unwrap_or(false)
    }

    pub fn load(filename: &str) -> Result<Config, ConfigError> {
        File::open(filename)
            .map_err(|err| ConfigError::IOError(err.to_string()))
            .and_then(|mut file| {
                let mut contents = String::new();
                file.read_to_string(&mut contents)
                    .map_err(|err| ConfigError::IOError(err.to_string()))
                    .and_then(|_| toml::from_str(&contents).map_err(ConfigError::from))
            })
            .and_then(|c: Config| c.validate())
    }

    fn validate(self) -> Result<Self, ConfigError> {
        let http_enabled = self.http_enabled();
        let https_enabled = self.https_enabled();

        if !http_enabled && !https_enabled {
            return Err(ConfigError::ValidateError(
                "listen address http and/or https must be defined".to_string(),
            ));
        }

        if https_enabled {
            if self.ssl_key_file.is_none() || self.ssl_key_file.as_ref().unwrap().is_empty() {
                return Err(ConfigError::ValidateError(
                    "HTTPS enabled but ssl_key_file is missing".to_string(),
                ));
            }
            if self.ssl_cert_file.is_none() || self.ssl_cert_file.as_ref().unwrap().is_empty() {
                return Err(ConfigError::ValidateError(
                    "HTTPS enabled but ssl_cert_file is missing".to_string(),
                ));
            }
        }

        if self.services.is_empty() {
            return Err(ConfigError::ValidateError("no service defined".to_string()));
        }

        let mut service_names: HashSet<String> = HashSet::with_capacity(self.services.len());

        for s in &self.services {
            s.validate().map_err(|err| ConfigError::ServiceConfError {
                service: s.name.clone(),
                error: err.to_string(),
            })?;

            // check if service name is unique
            if service_names.contains(&s.name) {
                return Err(ConfigError::ServiceConfError {
                    service: s.name.clone(),
                    error: "service name is not unique".to_string(),
                });
            }
            service_names.insert(s.name.clone());
        }

        Ok(self)
    }

    fn default_max_payload() -> usize {
        1048576
    }
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Service {
    /// service name (unique)
    pub name: String,

    /// service path
    pub path: String,

    /// accepted methods
    pub methods: Option<StringOrList>,

    /// request conditions
    pub request: Option<Request>,

    /// response definition
    pub response: Response,

    /// authorization & security
    pub auth: Option<Authorization>,

    /// service constants
    pub locals: Option<HashMap<String, serde_json::Value>>,

    /// script:function to call before generate response
    pub script: Option<String>,
}

impl Service {
    fn validate(&self) -> Result<(), ConfigError> {
        self.response.validate()
    }
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Authorization {
    /// service don't accept http connections
    #[serde(default)]
    pub https_required: bool,

    /// required client certificates serial number
    pub peer_cert_sn: Option<StringOrList>,
    /// required issuers common name
    pub peer_cert_issuer_cn: Option<StringOrList>,
    /// required subjects common name
    pub peer_cert_subject_cn: Option<StringOrList>,

    /// acceptable sources addresses / networks
    pub remote_address_ip: Option<StringOrList>,

    /// required user:pass
    pub basic_auth: Option<StringOrList>,
}

#[derive(Debug, Deserialize, Clone, Serialize)]
pub struct Request {
    pub content_type: Option<StringOrList>,
    // headers as `key: val` or object
    pub headers: Option<Vec<Header>>,

    /// match reqest body by regex
    pub body: Option<StringOrList>,

    /// match request json by path and regex
    pub json: Option<StringOrList>,
}

#[allow(clippy::enum_variant_names)]
#[derive(Error, Debug, Clone)]
pub enum ConfigError {
    #[error("load config file error: {0}")]
    IOError(String),

    #[error("parse config file error: {0}")]
    ParseError(String),

    #[error("load config file error: {0}")]
    ValidateError(String),

    #[error("configuration service `{service}`  error: {error}")]
    ServiceConfError { service: String, error: String },
}

impl From<toml::de::Error> for ConfigError {
    fn from(err: toml::de::Error) -> Self {
        ConfigError::ParseError(err.to_string())
    }
}
