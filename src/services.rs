//
// routes.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//
use super::conf;
use actix_web::{http, HttpResponse};
use regex::Regex;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fmt;
use std::str::FromStr;
use std::sync::Arc;

use crate::context::ConnectionContext;
use crate::errors::*;
use crate::matchers::*;
use crate::responders::Response;
use crate::scripting::{Script, ScriptError};

/// Definition of service
#[derive(Debug, Clone)]
pub struct Service {
    /// service name
    pub name: String,
    matchers: Arc<Vec<Box<dyn Matcher>>>,
    response: Response,
    /// local variables
    pub locals: Option<HashMap<String, serde_json::Value>>,

    path_re: Option<Regex>,

    rhai: Option<Script>,
}

/// Service is readonly object
unsafe impl Send for Service {}

fn build_matchers(c: &conf::Service) -> Result<Vec<Box<dyn Matcher>>, String> {
    let mut matchers: Vec<Box<dyn Matcher>> = vec![Box::new(
        ServicePath::from_str(c.path.as_ref()).map_err(|err| err.to_string())?,
    )];

    if let Some(rp) = &c.request {
        if let Some(headers) = &rp.headers {
            for header in headers.iter() {
                let header = Header::try_from(header)
                    .map_err(|err| format!("parse request headers failed: {}", err))?;
                matchers.push(Box::new(header));
            }
        }

        if let Some(types) = &rp.content_type {
            matchers.push(Box::new(
                ContentType::try_from(types.iter())
                    .map_err(|err| format!("request.content-type: {}", err))?,
            ));
        }

        if let Some(rbs) = &rp.body {
            for rb in rbs.iter() {
                matchers.push(Box::new(
                    BodyContent::from_str(rb.as_ref())
                        .map_err(|err| format!("request.body: {}", err))?,
                ));
            }
        }

        if let Some(rjs) = &rp.json {
            for rj in rjs.iter() {
                matchers.push(Box::new(
                    JsonPath::from_str(rj.as_ref())
                        .map_err(|err| format!("request.json: {}", err))?,
                ));
            }
        }
    }

    if let Some(auth) = &c.auth {
        if auth.https_required {
            matchers.push(Box::new(ConnInfo::HttpsRequired));
        }
        if let Some(pcn) = &auth.peer_cert_sn {
            matchers.push(Box::new(
                ConnInfo::try_from("peer_cert_sn", pcn.iter())
                    .map_err(|err| format!("request.peer_cert_sn: {}", err))?,
            ));
        }
        if let Some(pcic) = &auth.peer_cert_issuer_cn {
            matchers.push(Box::new(
                ConnInfo::try_from("peer_cert_issuer_cn", pcic.iter())
                    .map_err(|err| format!("request.peer_cert_issuer_cn: {}", err))?,
            ));
        }
        if let Some(pcsn) = &auth.peer_cert_subject_cn {
            matchers.push(Box::new(
                ConnInfo::try_from("peer_cert_subject_cn", pcsn.iter())
                    .map_err(|err| format!("request.peer_cert_subject_cn: {}", err))?,
            ));
        }

        if let Some(pcn) = &auth.remote_address_ip {
            matchers.push(Box::new(
                ConnInfo::try_from("remote_address_ip", pcn.iter())
                    .map_err(|err| format!("request.remote_address_ip: {}", err))?,
            ));
        }

        if let Some(users) = &auth.basic_auth {
            matchers
                .push(Box::new(BasicAuth::try_from(users.iter()).map_err(
                    |err| format!("request.remote_address_ip: {}", err),
                )?));
        }
    }

    if let Some(methods) = &c.methods {
        matchers.push(Box::new(
            Methods::try_from(methods.iter()).map_err(|err| format!("metrods: {}", err))?,
        ));
    }

    Ok(matchers)
}

impl TryFrom<&conf::Service> for Service {
    type Error = ConfigreServiceError;

    fn try_from(c: &conf::Service) -> Result<Self, Self::Error> {
        let matchers =
            build_matchers(c).map_err(|err| ConfigreServiceError::new(c.name.clone(), err))?;

        let path_re = Regex::new(&c.path).ok();

        let rhai = if let Some(s) = &c.script {
            Some(
                Script::from_str(s)
                    .map_err(|err| ConfigreServiceError::new(c.name.clone(), err.to_string()))?,
            )
        } else {
            None
        };

        let srv = Service {
            name: c.name.to_string(),
            matchers: Arc::new(matchers),
            response: Response::try_from(c).map_err(|err| {
                ConfigreServiceError::new(
                    c.name.clone(),
                    format!("configure response error: {}", err),
                )
            })?,
            locals: c.locals.clone(),
            path_re,
            rhai,
        };

        Ok(srv)
    }
}

impl Service {
    /// check if service match connection context
    pub fn is_match(&self, ctx: &ConnectionContext) -> bool {
        if log_enabled!(log::Level::Debug) {
            debug!("service {:}", &self.name);
            self.matchers
                .iter()
                .map(|m| {
                    if m.is_match(ctx) {
                        trace!("  matcher {:?} accept", m);
                        true
                    } else {
                        debug!("  matcher {:?} failed", m);
                        false
                    }
                })
                .all(|m| m)
        } else {
            self.matchers.iter().all(|m| m.is_match(ctx))
        }
    }

    /// process context and build response
    pub async fn build_response(&self, ctx: ConnectionContext) -> HttpResponse {
        let mut ctx = ctx;
        ctx.service_name = Some(self.name.clone());
        // create copy of locals
        if let Some(loc) = &self.locals {
            ctx.locals
                .extend(loc.iter().map(|(k, v)| (k.clone(), v.clone())));
        }

        // add captures from path into `path_cap`
        if let Some(re) = &self.path_re {
            if let Some(caps) = re.captures(&ctx.path) {
                let locals: HashMap<String, String> = re
                    .capture_names()
                    .filter_map(|cn| cn.map(|c| (c.to_string(), caps.name(c))))
                    .filter_map(|(cn, val)| val.map(|v| (cn, v.as_str().to_string())))
                    .collect::<HashMap<String, String>>();
                if !locals.is_empty() {
                    ctx.path_cap = locals;
                }
            }
        }

        let ctx = if let Some(script) = &self.rhai {
            trace!("evaluate function: {:?}", script);
            // evaluate script; return new context
            match script.run(ctx) {
                Ok(result) => result,
                Err(ScriptError::UserError {
                    filename,
                    function,
                    position,
                    result,
                }) => {
                    info!(
                        "user script error: file: {}, function: {}, position: {}, result: {:?}",
                        filename, function, position, &result
                    );
                    let mut resp = HttpResponse::build(make_status_code(result.status));
                    result.headers.iter().for_each(|(k, v)| {
                        resp.header(k, v.clone());
                    });
                    return resp.body(result.result);
                }
                Err(err) => {
                    error!("script error: {}", &err);
                    return HttpResponse::build(http::StatusCode::INTERNAL_SERVER_ERROR)
                        .body(err.to_string());
                }
            }
        } else {
            ctx
        };

        trace!("build response; ctx: {:?}", ctx);
        self.response.build_response(ctx, &self.name).await
    }
}

impl fmt::Display for Service {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "Service {}", self.name)?;
        writeln!(f, "  matchers: {:?}", self.matchers)?;
        writeln!(f, "  response: {}", self.response)?;
        writeln!(f, "  locals: {:?}", self.locals)?;
        if let Some(script) = &self.rhai {
            writeln!(f, "  script: {:?}", script)?;
        }

        Ok(())
    }
}

fn make_status_code(status: u16) -> http::StatusCode {
    http::StatusCode::from_u16(status).unwrap_or_else(|err| {
        warn!("invalid status code `{:?}`: {}", status, err);
        http::StatusCode::INTERNAL_SERVER_ERROR
    })
}
