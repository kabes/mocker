//
// middlewares.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//

use std::collections::HashSet;
use std::iter::FromIterator;
use std::pin::Pin;
use std::rc::Rc;
use std::task::{Context, Poll};

use actix_service::{Service, Transform};
use actix_web::dev::{ServiceRequest, ServiceResponse};
use actix_web::{Error, HttpMessage, HttpResponse};
use futures::future::{ok, Ready};
use futures::Future;

use crate::context::ConnectionInfo;

/// Authenticate client by certificate
pub struct Authenticator(Rc<Inner>);

impl Authenticator {
    pub fn new(cns: Option<Vec<String>>) -> Authenticator {
        Authenticator(Rc::new(match cns {
            None => Inner {
                enabled: false,
                accepted_cn: None,
            },
            Some(cns) if cns.is_empty() => Inner {
                enabled: false,
                accepted_cn: None,
            },
            Some(cns) => Inner {
                enabled: !cns.is_empty(),
                accepted_cn: Some(HashSet::from_iter(cns)),
            },
        }))
    }
}

#[derive(Clone)]
struct Inner {
    enabled: bool,
    accepted_cn: Option<HashSet<String>>,
}

impl Inner {
    fn need_auth(&self, p: &str) -> bool {
        self.enabled && !(p == "/metrics" || p == "/_services" || p == "/_health")
    }

    fn is_accepted(&self, ci: &ConnectionInfo) -> bool {
        // don't check certificates on http connection
        if !ci.ssl {
            return true;
        }

        let acs = self.accepted_cn.as_ref();
        if !ci.peer_cert_issuer_cn.is_empty()
            && acs
                .map(|acn| acn.contains(&ci.peer_cert_issuer_cn))
                .unwrap_or_default()
        {
            return true;
        }

        if let Some(san) = ci.peer_cert_subject_san.as_ref() {
            return san
                .iter()
                .any(|s| acs.map(|acn| acn.contains(s)).unwrap_or_default());
        }

        false
    }
}

// `S` - type of the next service
// `B` - type of response's body
impl<S, B> Transform<S> for Authenticator
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = AuthenticatorMiddleware<S>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(AuthenticatorMiddleware {
            service,
            inner: self.0.clone(),
        })
    }
}

pub struct AuthenticatorMiddleware<S> {
    service: S,
    inner: Rc<Inner>,
}

impl<S, B> Service for AuthenticatorMiddleware<S>
where
    S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Request = ServiceRequest;
    type Response = ServiceResponse<B>;
    type Error = Error;
    #[allow(clippy::type_complexity)]
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    fn call(&mut self, req: ServiceRequest) -> Self::Future {
        if self.inner.need_auth(req.path()) {
            let accepted = req
                .extensions()
                .get::<ConnectionInfo>()
                .map(|ci| self.inner.is_accepted(ci))
                .unwrap_or_default();
            if !accepted {
                warn!("conn not accepted");
                return Box::pin(ok(req.into_response(
                    HttpResponse::Forbidden()
                        .body("not accepted client certificate")
                        .into_body(),
                )));
            }
        };

        let fut = self.service.call(req);
        Box::pin(async move { fut.await })
    }
}
