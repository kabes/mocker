//
// main.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//
//
#![feature(trait_alias)]
#![feature(box_patterns)]

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate handlebars;
extern crate handlebars_misc_helpers;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate async_trait;
extern crate base64;
extern crate clap;
extern crate regex;
extern crate serde_json;
extern crate thiserror;
extern crate toml;
extern crate x509_parser;
#[macro_use]
extern crate der_parser;
extern crate actix_service;
extern crate rhai;
extern crate webpki;

use actix_tls::rustls::{ServerConfig, Session, TlsStream};
use actix_web::dev::Extensions;
use actix_web::middleware::Logger;
use actix_web::rt::net::TcpStream;
use actix_web::{get, route, web, App, HttpRequest, HttpResponse, HttpServer, Responder};
use futures::StreamExt;
use rustls::internal::pemfile::{certs, pkcs8_private_keys, rsa_private_keys};
use rustls::{AllowAnyAuthenticatedClient, NoClientAuth, RootCertStore};
use std::convert::TryFrom;
use std::{any::Any, fs::File, io::BufReader};
use x509_parser::prelude::*;

mod appstate;
mod conf;
mod context;
mod errors;
mod hb;
mod iputils;
mod json;
mod matchers;
mod metrics;
mod middlewares;
mod responders;
mod scripting;
mod services;
mod tls;

use appstate::AppState;

#[get("/_health")]
async fn handle_health() -> HttpResponse {
    HttpResponse::Ok().finish()
}

#[get("/_services")]
async fn handle_services(data: web::Data<AppState>) -> impl Responder {
    let info = data
        .services
        .iter()
        .map(|s| format!("{}", s))
        .collect::<Vec<String>>()
        .join("\n");
    HttpResponse::Ok().content_type("plain/text").body(info)
}

#[route(
    "/.*",
    method = "GET",
    method = "POST",
    method = "DELETE",
    method = "PUT"
)]
async fn handle_root(
    req: HttpRequest,
    data: web::Data<AppState>,
    mut payload: web::Payload,
    conn_info: web::ReqData<context::ConnectionInfo>,
) -> impl Responder {
    let req_path = req.path().to_string();
    let method = req.method().as_str().to_lowercase();
    let conn_info = conn_info.into_inner();
    debug!(
        "{} {} {:?} {:?}",
        method,
        req_path,
        req.headers(),
        conn_info
    );

    let mut body = web::BytesMut::new();
    while let Some(chunk) = payload.next().await {
        let chunk = chunk.unwrap();
        // limit max size of in-memory payload
        if (body.len() + chunk.len()) > data.max_payload {
            return HttpResponse::BadRequest().body("payloud too large");
        }
        body.extend_from_slice(&chunk);
    }

    let response = {
        let globals = data.globals.clone();
        let ctx = context::ConnectionContext::new(&req, body, globals, conn_info);
        match data.services.iter().find(|s| s.is_match(&ctx)) {
            Some(service) => {
                metrics::update_http_serv_counter(&service.name, &method);
                service.build_response(ctx).await
            }
            None => {
                info!("not found hander for {} {}", method, req_path,);
                HttpResponse::NotFound().body(format!("Not found: {}", req.path()))
            }
        }
    };

    response
}

fn setup_logging(level: u64) {
    let level = match level {
        0 => log::LevelFilter::Warn,
        1 => log::LevelFilter::Info,
        2 => log::LevelFilter::Debug,
        _ => log::LevelFilter::Trace,
    };

    env_logger::Builder::new()
        .target(env_logger::Target::Stdout)
        .filter_level(level)
        .init();
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let cli = cli_matches();
    setup_logging(cli.occurrences_of("v"));

    info!("starting");

    let conf_filename = cli.value_of("config").unwrap_or("mocker.toml");
    info!("loading configuration file {}", conf_filename);
    let conf = match conf::Config::load(conf_filename) {
        Ok(conf) => conf,
        Err(err) => {
            error!("{}", err.to_string());
            return Ok(());
        }
    };
    debug!("conf {:?}", conf);

    if let Some(rhai_helpers) = &conf.rhai_helpers {
        trace!("setup rhai helpers");
        if let Err(err) = hb::register_rhai_helpers(rhai_helpers) {
            error!("{}", err);
            return Ok(());
        }
    }

    trace!("setup services");
    let services = &conf.services;
    let services: Result<Vec<services::Service>, _> =
        services.iter().map(services::Service::try_from).collect();
    let services = match services {
        Ok(r) => r,
        Err(err) => {
            error!("{}", err);
            return Ok(());
        }
    };

    if log_enabled!(log::Level::Trace) {
        services.iter().for_each(|r| trace!("{:#?}", r));
    } else if log_enabled!(log::Level::Debug) {
        services.iter().for_each(|r| debug!("{}", r));
    } else if log_enabled!(log::Level::Info) {
        services.iter().for_each(|r| info!("Service: {}", r.name));
    }

    let max_payload = conf.max_payload;

    let globals = conf
        .globals
        .as_ref()
        .cloned()
        .unwrap_or_else(std::collections::HashMap::new);

    let client_cns = if conf.ssl_require_client_cert {
        conf.ssl_client_cn.clone()
    } else {
        None
    };

    let server = HttpServer::new(move || {
        let logger = Logger::new(
            r#"%a %t "%r" %s %b "%{Referer}i" "%{User-Agent}i" %T %{X-MOCKER-SERVICE}o"#,
        );
        App::new()
            .wrap(logger)
            .service(metrics::handle_metrics)
            .service(
                web::scope("/_")
                    .service(handle_health)
                    .service(handle_services),
            )
            .service(
                web::scope("/")
                    .data(AppState {
                        services: services.clone(),
                        max_payload,
                        globals: globals.clone(),
                    })
                    .wrap(middlewares::Authenticator::new(
                        client_cns.as_ref().map(|c| c.inner()),
                    ))
                    .service(handle_root),
            )
    })
    .on_connect(get_connection_info);

    let server = if conf.server_workers > 0 {
        server.workers(conf.server_workers)
    } else {
        server
    };

    let server = if let Some(addr) = &conf.https_address {
        info!("HTTPS enabled: {}", addr);
        match build_ssl_builder(&conf) {
            Err(err) => {
                error!("prepare ssl error: {}", err);
                return Ok(());
            }
            Ok(builder) => server.bind_rustls(addr, builder)?,
        }
    } else {
        server
    };

    let server = if let Some(addr) = &conf.http_address {
        info!("HTTP enabled: {}", addr);
        server.bind(addr)?
    } else {
        server
    };

    server.run().await
}

const SAN_OID: der_parser::oid::Oid<'static> = oid!(2.5.29 .17);

fn log_client_cert(cert: &x509_parser::certificate::X509Certificate) -> String {
    let subject = &cert.tbs_certificate.subject;
    let issuer = &cert.tbs_certificate.issuer;
    format!(
        "client certificate Subject: {}, Issuer: {}, Serial: {}, {:?}",
        subject,
        issuer,
        cert.tbs_certificate.raw_serial_as_string(),
        cert.validity(),
    )
}

fn cert_get_san(c: &x509_parser::certificate::X509Certificate) -> Option<Vec<String>> {
    if let Some(ext) = c.extensions().get(&SAN_OID) {
        let parsed_ext = ext.parsed_extension();
        if let x509_parser::extensions::ParsedExtension::SubjectAlternativeName(san) = parsed_ext {
            let san: Vec<String> = san
                .general_names
                .iter()
                .map(|s| match s {
                    GeneralName::DNSName(r) => r.to_string(),
                    GeneralName::IPAddress(ip) => {
                        format!("{}.{}.{}.{}", ip[0], ip[1], ip[2], ip[3])
                    }
                    _ => "".to_string(),
                })
                .filter(|s| !s.is_empty())
                .collect();
            if !san.is_empty() {
                return Some(san);
            }
        }
    }
    None
}

fn get_connection_info(connection: &dyn Any, data: &mut Extensions) {
    let mut cn = context::ConnectionInfo {
        ..Default::default()
    };
    if let Some(tls_socket) = connection.downcast_ref::<TlsStream<TcpStream>>() {
        let (socket, tls_session) = tls_socket.get_ref();
        cn.remote_address_ip = socket.peer_addr().unwrap().ip();
        if log_enabled!(log::Level::Debug) {
            debug!(
                "{:?} SNI:{:?} Prot:{:?} Ciphers:{:?}",
                &socket,
                &tls_session.get_sni_hostname(),
                &tls_session.get_protocol_version(),
                &tls_session.get_negotiated_ciphersuite(),
            );
        }

        if let Some(mut certs) = tls_session.get_peer_certificates() {
            let cert = certs.remove(0);
            if let Ok((_, c)) = parse_x509_certificate(&cert.0.to_vec()) {
                if log_enabled!(log::Level::Debug) {
                    debug!("{}", log_client_cert(&c));
                }
                let subject = &c.tbs_certificate.subject;
                cn.peer_cert_subject_cn = subject
                    .iter_common_name()
                    .next()
                    .and_then(|cn| cn.as_str().ok())
                    .unwrap_or_default()
                    .to_string();

                let issuer = &c.tbs_certificate.issuer;
                cn.peer_cert_issuer_cn = issuer
                    .iter_common_name()
                    .next()
                    .and_then(|cn| cn.as_str().ok())
                    .unwrap_or_default()
                    .to_string();

                //san

                cn.peer_cert_sn = c.tbs_certificate.raw_serial_as_string();
                cn.peer_cert_subject_san = cert_get_san(&c);
            }
        }
        cn.ssl = true;
    }

    if let Some(stream) = connection.downcast_ref::<TcpStream>() {
        cn.remote_address_ip = stream.peer_addr().unwrap().ip();
    }

    data.insert(cn);
}

fn build_ssl_builder(conf: &conf::Config) -> Result<ServerConfig, String> {
    let mut cert_store = RootCertStore::empty();

    if let Some(cacerts) = &conf.ssl_ca_certs_file {
        debug!("loading ca certs from {:?}", cacerts);
        let ca_cert = &mut BufReader::new(File::open(cacerts).map_err(|err| err.to_string())?);
        cert_store
            .add_pem_file(ca_cert)
            .map_err(|err| format!("add certificates {} error: {:?}", cacerts, err))?;
    }

    let mut config = if conf.ssl_require_client_cert {
        info!("ssl required trusted client certificate");
        ServerConfig::new(AllowAnyAuthenticatedClient::new(cert_store))
    } else {
        ServerConfig::new(NoClientAuth::new())
    };

    // import server cert and key
    let cert = conf.ssl_cert_file.as_ref().unwrap();
    let cert_file = &mut BufReader::new(
        File::open(cert).map_err(|err| format!("open cert file {} error {}", cert, err))?,
    );

    let key = conf.ssl_key_file.as_ref().unwrap();
    let key_file = &mut BufReader::new(
        File::open(std::path::Path::new(key))
            .map_err(|err| format!("open key file {} error {}", key, err))?,
    );

    let cert_chain = certs(cert_file).unwrap();
    let mut keys = rsa_private_keys(key_file).unwrap();
    keys.extend(pkcs8_private_keys(key_file).unwrap());
    config.set_single_cert(cert_chain, keys.remove(0)).unwrap();

    Ok(config)
}

fn cli_matches<'a>() -> clap::ArgMatches<'a> {
    let matches = clap::App::new("mocker")
        .version("0.1")
        .author("Karol Będkowski")
        .about("Mock http services")
        .arg(
            clap::Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("CONFIG_FILE")
                .help("Sets a custom config file")
                .env("CONFIG_FILE")
                .default_value("mocker.toml")
                .takes_value(true),
        )
        .arg(
            clap::Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();
    matches
}
