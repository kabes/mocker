//
// tls.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//

use rustls::Certificate;
use rustls::RootCertStore;
use rustls::ServerCertVerified;
use rustls::ServerCertVerifier;
use rustls::TLSError;

pub struct NoServerCertVerifier {}

/// Server certificate verifier that accept everything
impl ServerCertVerifier for NoServerCertVerifier {
    fn verify_server_cert(
        &self,
        _roots: &RootCertStore,
        _presented_certs: &[Certificate],
        _dns_name: webpki::DNSNameRef,
        _ocsp_response: &[u8],
    ) -> Result<ServerCertVerified, TLSError> {
        Ok(ServerCertVerified::assertion())
    }
}
