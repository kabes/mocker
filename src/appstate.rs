//
// appstate.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//

use crate::services;
use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct AppState {
    pub services: Vec<services::Service>,
    pub max_payload: usize,
    pub globals: HashMap<String, serde_json::Value>,
}
