//
// metrics.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//

use actix_web::{get, HttpResponse};
use prometheus::{self, register_int_counter_vec, Encoder, IntCounterVec, TextEncoder};

lazy_static! {
    static ref HTTP_SRV_COUNTER: IntCounterVec = register_int_counter_vec!(
        "mocker_http_requests_total",
        "Total number of HTTP requests made for service.",
        &["service", "method"]
    )
    .unwrap();
}

#[get("/metrics")]
pub async fn handle_metrics() -> HttpResponse {
    let mut buffer = vec![];
    TextEncoder::new()
        .encode(&prometheus::gather(), &mut buffer)
        .unwrap();
    HttpResponse::Ok().body(buffer)
}

pub fn update_http_serv_counter(service: &str, method: &str) {
    HTTP_SRV_COUNTER.with_label_values(&[service, method]).inc();
}
