//
// matchers.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//
use crate::conf;
use crate::context::ConnectionContext;
use crate::errors::PrepareError;
use crate::iputils;
use crate::json;
use core::slice::Iter;
use regex::Regex;
use std::convert::TryFrom;
use std::fmt;
use std::str::FromStr;

#[derive(Debug, Clone)]
enum StringMatcher {
    Plain(String),
    Prefix(String),
    Re(Regex),
}

impl fmt::Display for StringMatcher {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), fmt::Error> {
        match self {
            StringMatcher::Plain(v) => write!(f, "`{}`", v),
            StringMatcher::Prefix(v) => write!(f, "prefix:`{}`", v),
            StringMatcher::Re(v) => write!(f, "re:`{}`", v),
        }
    }
}

impl StringMatcher {
    fn new(s: &str) -> Self {
        if s.ends_with('*') {
            let s = s.trim_end_matches('*');
            StringMatcher::Prefix(s.to_owned())
        } else {
            StringMatcher::Plain(s.to_owned())
        }
    }

    fn new_re(s: &str) -> Result<Self, PrepareError> {
        Ok(StringMatcher::Re(Regex::new(s).map_err(|err| {
            PrepareError::new(&format!("compile `{}` to regex error: {}", s, err))
        })?))
    }

    fn is_match(&self, s: &str) -> bool {
        match self {
            StringMatcher::Plain(v) => v == s,
            StringMatcher::Prefix(p) => s.starts_with(p),
            StringMatcher::Re(re) => re.is_match(s),
        }
    }
}

pub trait Matcher: std::fmt::Debug {
    fn is_match(&self, ctx: &ConnectionContext) -> bool;

    fn need_body(&self) -> bool {
        false
    }
}

/// Service path matcher
#[derive(Clone)]
pub struct ServicePath(StringMatcher);

impl FromStr for ServicePath {
    type Err = PrepareError;

    fn from_str(c: &str) -> Result<Self, Self::Err> {
        let re = if c.starts_with('^') {
            StringMatcher::new_re(c)
        } else {
            let c = String::from("^") + c;
            StringMatcher::new_re(&c)
        };
        Ok(match re {
            Ok(re) => ServicePath(re),
            Err(err) => {
                info!("compile `{}` to regex error: {}", c, err);
                ServicePath(StringMatcher::new(c))
            }
        })
    }
}

impl Matcher for ServicePath {
    fn is_match(&self, ctx: &ConnectionContext) -> bool {
        self.0.is_match(&ctx.path)
    }
}

impl fmt::Debug for ServicePath {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("ServicePath").field(&self.0).finish()
    }
}

/// Match http method (must be on the list)
#[derive(Debug, Clone, Serialize)]
pub struct Methods(Vec<String>);

impl TryFrom<Iter<'_, String>> for Methods {
    type Error = PrepareError;

    fn try_from(m: Iter<String>) -> Result<Self, Self::Error> {
        let methods = m.map(|m| m.to_uppercase()).collect();
        Ok(Methods(methods))
    }
}

impl Matcher for Methods {
    fn is_match(&self, ctx: &ConnectionContext) -> bool {
        self.0.is_empty() || self.0.contains(&ctx.method.to_string())
    }
}

/// Match headers (all headers in list must be in request)
#[derive(Debug, Clone)]
pub struct Header((String, StringMatcher));

impl TryFrom<&conf::Header> for Header {
    type Error = PrepareError;

    fn try_from(c: &conf::Header) -> Result<Self, Self::Error> {
        let c = c.inner();
        if c.handlebars {
            warn!(
                "handlebars templates not supported in request headers ({})",
                c
            );
        }
        Ok(Header((
            c.key.trim().to_lowercase(),
            StringMatcher::new(c.value.trim()),
        )))
    }
}

impl Matcher for Header {
    fn is_match(&self, ctx: &ConnectionContext) -> bool {
        let k = &self.0 .0;
        let v = &self.0 .1;
        let res = ctx
            .headers
            .get(k)
            .map(|val| v.is_match(val))
            .unwrap_or_default();
        if !res {
            debug!("required header {}=`{}` not found", k, v);
        }
        res
    }
}

/// Match request body by regex
#[derive(Debug, Clone)]
pub struct BodyContent(Regex);

impl FromStr for BodyContent {
    type Err = PrepareError;

    fn from_str(c: &str) -> Result<Self, Self::Err> {
        match Regex::new(c) {
            Ok(re) => Ok(BodyContent(re)),
            Err(err) => Err(PrepareError::GenericError(format!(
                "compile `{}` to regex error: {}",
                c, err
            ))),
        }
    }
}

impl Matcher for BodyContent {
    fn is_match(&self, ctx: &ConnectionContext) -> bool {
        self.0.is_match(&ctx.req_body)
    }
}

/// Match request json by patch
#[derive(Debug, Clone)]
pub struct JsonPath {
    path: String,
    re: Regex,
}

impl FromStr for JsonPath {
    type Err = PrepareError;

    fn from_str(c: &str) -> Result<Self, Self::Err> {
        let (path, re) = c
            .split_once(':')
            .ok_or_else(|| PrepareError::GenericError(format!("invalid json match: `{}`", c)))?;

        let path = path.trim();
        if path.is_empty() {
            return Err(PrepareError::GenericError(format!(
                "invalid json match: `{}`; missing path",
                c
            )));
        }
        let re = re.trim();
        if re.is_empty() {
            return Err(PrepareError::GenericError(format!(
                "invalid json match: `{}`; missing regex",
                c
            )));
        }

        match Regex::new(re) {
            Ok(re) => Ok(JsonPath {
                path: path.to_owned(),
                re,
            }),
            Err(err) => Err(PrepareError::GenericError(format!(
                "compile `{}` to regex error: {}",
                c, err
            ))),
        }
    }
}

impl Matcher for JsonPath {
    fn is_match(&self, ctx: &ConnectionContext) -> bool {
        ctx.req_json
            .as_ref()
            .and_then(|jb| json::json_query_to_str(jb, &self.path))
            .map(|v| self.re.is_match(&v))
            .unwrap_or_default()
    }
}

#[derive(Debug, Clone)]
pub enum ConnInfo {
    HttpsRequired,
    CertSn(Vec<String>),
    CertIssuerCn(Vec<String>),
    CertSubjectCn(Vec<String>),
    RemoteIp(Vec<iputils::IpMatcher>),
}

impl ConnInfo {
    pub fn try_from(part: &str, values: Iter<String>) -> Result<Self, PrepareError> {
        let values = values.map(|v| v.trim().to_string()).collect();
        match part {
            "peer_cert_sn" => Ok(ConnInfo::CertSn(values)),
            "peer_cert_issuer_cn" => Ok(ConnInfo::CertIssuerCn(values)),
            "peer_cert_subject_cn" => Ok(ConnInfo::CertSubjectCn(values)),
            "remote_address_ip" => {
                let ipn: Result<Vec<iputils::IpMatcher>, PrepareError> = values
                    .iter()
                    .map(|v| iputils::IpMatcher::try_from(v.as_ref()))
                    .collect();
                ipn.map(ConnInfo::RemoteIp)
            }
            _ => Err(PrepareError::GenericError(format!("invalid part {}", part))),
        }
    }
}

impl Matcher for ConnInfo {
    fn is_match(&self, ctx: &ConnectionContext) -> bool {
        match self {
            ConnInfo::HttpsRequired => ctx.connection.ssl,
            ConnInfo::CertSn(v) => {
                let val = &ctx.connection.peer_cert_sn;
                !val.is_empty() && v.contains(val)
            }
            ConnInfo::CertIssuerCn(v) => {
                let val = &ctx.connection.peer_cert_issuer_cn;
                !val.is_empty() && v.contains(val)
            }
            ConnInfo::CertSubjectCn(v) => {
                let val = &ctx.connection.peer_cert_subject_cn;
                (!val.is_empty() && v.contains(val))
                    || (ctx
                        .connection
                        .peer_cert_subject_san
                        .as_ref()
                        .map(|sans| sans.iter().any(|san| v.contains(san)))
                        .unwrap_or_default())
            }
            ConnInfo::RemoteIp(ips) => {
                let ip = ctx.connection.remote_address_ip;
                ips.iter().any(|v| v.is_match(ip))
            }
        }
    }
}

/// BasicAuth check is header `authorization` exists and have on on given values (after hash)
#[derive(Debug, Clone)]
pub struct BasicAuth(Vec<String>);

impl TryFrom<Iter<'_, String>> for BasicAuth {
    type Error = PrepareError;

    fn try_from(auths: Iter<String>) -> Result<Self, PrepareError> {
        let aut: Result<Vec<String>, PrepareError> = auths
            .map(|v| {
                v.split_once(':')
                    .map(|(user, pass)| {
                        let userpass = format!("{}:{}", user.trim(), pass.trim());
                        String::from("Basic ") + base64::encode(&userpass).as_ref()
                    })
                    .ok_or_else(|| PrepareError::new(&format!("invalid user:pass: {}", v)))
            })
            .collect();
        Ok(BasicAuth(aut?))
    }
}

impl Matcher for BasicAuth {
    fn is_match(&self, ctx: &ConnectionContext) -> bool {
        ctx.headers
            .get("authorization")
            .map(|a| self.0.contains(a))
            .unwrap_or_default()
    }
}

#[derive(Debug, Clone)]
pub struct ContentType(Vec<StringMatcher>);

impl TryFrom<Iter<'_, String>> for ContentType {
    type Error = PrepareError;

    fn try_from(types: Iter<String>) -> Result<Self, PrepareError> {
        Ok(ContentType(types.map(|t| StringMatcher::new(t)).collect()))
    }
}

impl Matcher for ContentType {
    fn is_match(&self, ctx: &ConnectionContext) -> bool {
        ctx.headers
            .get("content-type")
            .map(|a| self.0.iter().any(|ct| ct.is_match(a)))
            .unwrap_or_default()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::context;

    #[test]
    fn test_string_matcher_plain() {
        assert!(matches!(
            StringMatcher::new("abc"),
            StringMatcher::Plain(x) if x == "abc".to_string()
        ));
        assert!(StringMatcher::new("abc").is_match("abc"));
        assert!(!StringMatcher::new("abc").is_match(""));
        assert!(!StringMatcher::new("abc").is_match("ABC"));
        assert!(!StringMatcher::new("abc").is_match("abcd"));
    }

    #[test]
    fn test_string_matcher_prefix() {
        assert!(matches!(
            StringMatcher::new("abc*"),
            StringMatcher::Prefix(x) if x == "abc".to_string()
        ));
        assert!(matches!(
            StringMatcher::new("abc***"),
            StringMatcher::Prefix(x) if x == "abc".to_string()
        ));
        assert!(StringMatcher::new("abc*").is_match("abc"));
        assert!(!StringMatcher::new("abc*").is_match("ab"));
        assert!(!StringMatcher::new("abc*").is_match(""));
        assert!(!StringMatcher::new("abc*").is_match("ABC"));
        assert!(StringMatcher::new("abc*").is_match("abcd"));
        assert!(StringMatcher::new("abc*").is_match("abc d"));
        assert!(StringMatcher::new("abc**").is_match("abc d"));
    }

    #[test]
    fn test_string_matcher_re() {
        assert!(matches!(
            StringMatcher::new_re("abc[a-z]+"),
            Ok(StringMatcher::Re(_))
        ));
        assert!(StringMatcher::new_re("abc*").unwrap().is_match("abcccc"));
        assert!(!StringMatcher::new_re("abc[a-z]+")
            .unwrap()
            .is_match("abc123"));
    }

    #[test]
    fn test_methods_matcher() {
        let ctx_get = context::ConnectionContext {
            method: "GET".to_string(),
            ..Default::default()
        };
        let ctx_post = context::ConnectionContext {
            method: "POST".to_string(),
            ..Default::default()
        };

        let m1 = Methods::try_from([].iter()).unwrap();
        assert!(m1.is_match(&ctx_post));
        assert!(m1.is_match(&ctx_get));

        let m2 = Methods::try_from(["get".to_string()].iter()).unwrap();
        assert!(!m2.is_match(&ctx_post));
        assert!(m2.is_match(&ctx_get));

        let m3 = Methods::try_from(["get".to_string(), "POST".to_string()].iter()).unwrap();
        assert!(m3.is_match(&ctx_post));
        assert!(m3.is_match(&ctx_get));
    }

    #[test]
    fn test_headers_matcher() {
        let ctx = context::ConnectionContext {
            headers: [("x-aa", "abc"), ("x-bb", "123aas")]
                .iter()
                .map(|(a, b)| (a.to_string(), b.to_string()))
                .collect(),
            ..Default::default()
        };

        let header = conf::Header::new(conf::TemplatableKeyValue::from_str("x-aa: abc").unwrap());
        let hm = Header::try_from(&header).unwrap();
        assert!(hm.is_match(&ctx));

        let header = conf::Header::new(conf::TemplatableKeyValue::from_str("x-bb: 123*").unwrap());
        let hm = Header::try_from(&header).unwrap();
        assert!(hm.is_match(&ctx));

        let header = conf::Header::new(conf::TemplatableKeyValue::from_str("x-aa: aca").unwrap());
        let hm = Header::try_from(&header).unwrap();
        assert!(!hm.is_match(&ctx));

        let header = conf::Header::new(conf::TemplatableKeyValue::from_str("x-bb: 1233*").unwrap());
        let hm = Header::try_from(&header).unwrap();
        assert!(!hm.is_match(&ctx));
    }
}
