//
// errors.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//
use std::fmt;
use thiserror::Error;

#[derive(Error, Debug, Clone)]
pub struct ConfigreServiceError {
    service: String,
    error: String,
}

impl fmt::Display for ConfigreServiceError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(
            f,
            "configure service `{}` error: {}",
            &self.service, &self.error
        )
    }
}

impl ConfigreServiceError {
    pub fn new(service: String, err: String) -> Self {
        ConfigreServiceError {
            service,
            error: err,
        }
    }
}

#[allow(clippy::enum_variant_names)]
#[derive(Error, Debug, Clone)]
pub enum PrepareError {
    #[error("{0}")]
    GenericError(String),

    #[error("file `{0}` not exist")]
    MissingFile(String),

    #[error("parse template {template} error: {error}")]
    ParseTemplateError { template: String, error: String },

    #[error("load script {filename} error: {error}")]
    RhaiLoadError { filename: String, error: String },
}

impl PrepareError {
    pub fn new(err: &str) -> Self {
        PrepareError::GenericError(err.to_string())
    }
}

#[allow(clippy::enum_variant_names)]
#[derive(Error, Debug, Clone)]
pub enum ResponseError {
    #[error("{0}")]
    GenericError(String),

    #[error("open file {file} error: {error}")]
    IoFileError { file: String, error: String },

    #[error("render template {template} error: {error}")]
    TemplateError { template: String, error: String },

    #[error("{0}")]
    SSLConfigError(String),
}

impl From<actix_web::client::PayloadError> for ResponseError {
    fn from(err: actix_web::client::PayloadError) -> Self {
        ResponseError::GenericError(err.to_string())
    }
}

impl From<actix_web::client::SendRequestError> for ResponseError {
    fn from(err: actix_web::client::SendRequestError) -> Self {
        ResponseError::GenericError(err.to_string())
    }
}

impl From<&str> for ResponseError {
    fn from(err: &str) -> Self {
        ResponseError::GenericError(err.to_string())
    }
}
