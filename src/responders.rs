//
// reponsers.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//
use crate::context::ConnectionContext;
use crate::hb;
use crate::scripting::Script;
use crate::{conf, errors};
use actix_web::client::Connector;
use actix_web::dev::HttpResponseBuilder;
use actix_web::{http, HttpResponse};
use async_trait::async_trait;
use rustls::internal::pemfile::{certs, pkcs8_private_keys, rsa_private_keys};
use rustls::ClientConfig;
use std::convert::TryFrom;
use std::fmt;
use std::io::prelude::*;
use std::str::FromStr;
use std::sync::Arc;
use std::{fs::File, io::BufReader};

#[derive(Debug, Clone)]
enum HeaderValue {
    HbTemplate((String, String)),
    Plain((String, String)),
}

impl TryFrom<&conf::Header> for HeaderValue {
    type Error = errors::PrepareError;

    fn try_from(c: &conf::Header) -> Result<Self, Self::Error> {
        let c = c.inner();
        let key = c.key.trim().to_string();
        let value = c.value.trim().to_string();
        if c.handlebars {
            hb::register_text(&value)?;
            Ok(HeaderValue::HbTemplate((key, value)))
        } else {
            Ok(HeaderValue::Plain((key, value)))
        }
    }
}

impl HeaderValue {
    fn evaluate(&self, ctx: &ConnectionContext) -> Option<(String, String)> {
        match &self {
            HeaderValue::Plain((k, v)) => Some((k.to_owned(), v.to_owned())),
            HeaderValue::HbTemplate((k, tmpl)) => {
                hb::render(tmpl, ctx).ok().map(|v| (k.to_owned(), v))
            }
        }
    }
}

fn put_headers_in_resp(
    ctx: &ConnectionContext,
    headers: &Option<Vec<HeaderValue>>,
    builder: &mut HttpResponseBuilder,
) {
    if let Some(headers) = headers {
        // set headers
        headers.iter().for_each(|hv| {
            if let Some((name, value)) = hv.evaluate(ctx) {
                builder.header(&name, value);
            }
        });
    }
}

#[async_trait(?Send)]
pub trait Responder: std::fmt::Debug {
    async fn respond(
        &self,
        resp: &mut HttpResponseBuilder,
        ctx: &ConnectionContext,
    ) -> Result<HttpResponse, errors::ResponseError>;
}

/// Respond with empty body
#[derive(Debug, Clone)]
struct EmptyResponder {}

#[async_trait(?Send)]
impl Responder for EmptyResponder {
    async fn respond(
        &self,
        resp: &mut HttpResponseBuilder,
        _ctx: &ConnectionContext,
    ) -> Result<HttpResponse, errors::ResponseError> {
        Ok(resp.finish())
    }
}

/// Send file as response
#[derive(Debug, Clone)]
struct FileResponder(String);

impl FromStr for FileResponder {
    type Err = errors::PrepareError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if !std::path::Path::new(s).exists() {
            Err(errors::PrepareError::MissingFile(s.to_string()))
        } else {
            Ok(FileResponder(s.to_owned()))
        }
    }
}

#[async_trait(?Send)]
impl Responder for FileResponder {
    async fn respond(
        &self,
        resp: &mut HttpResponseBuilder,
        _ctx: &ConnectionContext,
    ) -> Result<HttpResponse, errors::ResponseError> {
        let data = File::open(&self.0).and_then(|mut file| {
            let mut contents = String::new();
            file.read_to_string(&mut contents).map(|_| contents)
        });

        match data {
            Ok(r) => Ok(resp.body(r)),
            Err(err) => Err(errors::ResponseError::IoFileError {
                file: self.0.to_owned(),
                error: err.to_string(),
            }),
        }
    }
}

/// Send static text as response
#[derive(Debug, Clone)]
struct TextResponder(String);

impl FromStr for TextResponder {
    type Err = errors::PrepareError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(TextResponder(s.to_owned()))
    }
}

#[async_trait(?Send)]
impl Responder for TextResponder {
    async fn respond(
        &self,
        resp: &mut HttpResponseBuilder,
        _ctx: &ConnectionContext,
    ) -> Result<HttpResponse, errors::ResponseError> {
        Ok(resp.body(&self.0))
    }
}

/// Send processed handlebars template (from file or text)
#[derive(Debug, Clone)]
pub struct HbResponder(String);

impl HbResponder {
    fn try_new_text(s: &str) -> Result<Self, errors::PrepareError> {
        hb::register_text(s)?;
        Ok(HbResponder(s.to_owned()))
    }

    fn try_new_file(s: &str) -> Result<Self, errors::PrepareError> {
        hb::register_file(s)?;
        Ok(HbResponder(s.to_owned()))
    }
}

#[async_trait(?Send)]
impl Responder for HbResponder {
    async fn respond(
        &self,
        resp: &mut HttpResponseBuilder,
        ctx: &ConnectionContext,
    ) -> Result<HttpResponse, errors::ResponseError> {
        hb::render(&self.0, ctx).map(|r| resp.body(&r))
    }
}

/// Ask remote service for response
#[derive(Clone)]
pub struct RemoteResponder {
    conf: conf::RemoteResponse,

    /// extra headers send to backend
    headers: Vec<(Vec<u8>, String)>,

    method: http::Method,

    // script to run on response
    rhai: Option<Script>,
}

impl fmt::Debug for RemoteResponder {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let headers: Vec<String> = self
            .headers
            .iter()
            .map(|(k, v)| {
                format!(
                    "{}: {}",
                    String::from_utf8(k.to_vec()).unwrap_or_default(),
                    &v
                )
            })
            .collect();

        f.debug_struct("RemoteResponder")
            .field("conf", &self.conf)
            .field("headers", &headers)
            .field("methods", &self.method)
            .finish()
    }
}

impl RemoteResponder {
    fn create_client_config(&self) -> Result<ClientConfig, errors::ResponseError> {
        let mut config = ClientConfig::new();
        if !self.conf.url.starts_with("https://") {
            return Ok(config);
        }

        // configure tls
        if self.conf.client_cert_file.is_some() {
            let cert_file = self.conf.client_cert_file.as_ref().unwrap();
            let cert_file = &mut BufReader::new(File::open(cert_file).map_err(|err| {
                errors::ResponseError::SSLConfigError(format!(
                    "load cert file {} error {}",
                    cert_file, err
                ))
            })?);

            let key_file = self.conf.client_key_file.as_ref().unwrap();
            let key_file =
                &mut BufReader::new(File::open(std::path::Path::new(key_file)).map_err(|err| {
                    errors::ResponseError::SSLConfigError(format!(
                        "load client key {} error {}",
                        key_file, err
                    ))
                })?);

            let cert_chain = certs(cert_file).unwrap();
            let mut keys = rsa_private_keys(key_file).unwrap();
            keys.extend(pkcs8_private_keys(key_file).unwrap());
            config
                .set_single_client_cert(cert_chain, keys.remove(0))
                .unwrap();
        }

        if let Some(ca) = self.conf.trusted_ca_file.as_ref() {
            let ca_cert = &mut BufReader::new(File::open(ca).map_err(|err| {
                errors::ResponseError::SSLConfigError(format!(
                    "load ca certs from {} error {}",
                    ca, err
                ))
            })?);
            config.root_store.add_pem_file(ca_cert).map_err(|err| {
                errors::ResponseError::SSLConfigError(format!(
                    "add ca certificate {} error {:?}",
                    ca, err
                ))
            })?;
        }

        if self.conf.insecure_skip_verify {
            // no verify server certificates
            config
                .dangerous()
                .set_certificate_verifier(Arc::new(crate::tls::NoServerCertVerifier {}));
        }

        Ok(config)
    }

    // fn process_response(
    //     &self,
    //     body: actix_web::web::Bytes,
    //     response: &actix_web::client::ClientResponse<
    //         actix_web::dev::Decompress<
    //             actix_web::dev::Payload<
    //                 std::pin::Pin<
    //                     Box<
    //                         dyn futures::Stream<
    //                             Item = Result<
    //                                 actix_web::web::Bytes,
    //                                 actix_web::client::PayloadError,
    //                             >,
    //                         >,
    //                     >,
    //                 >,
    //             >,
    //         >,
    //     >,
    // ) -> Result<actix_web::web::Bytes, errors::ResponseError> {
    //     if self.rhai.is_none() {
    //         return Ok(body);
    //     }

    //     if body.is_empty() {
    //         return Ok(body);
    //     }

    //     Ok(body)
    // }
}

// fn check_content_type(headers: &actix_web::http::HeaderMap, expected: &str) -> bool {
//     headers
//         .get("content-type")
//         .map(|h| h.to_str())
//         .map(|hv| hv.unwrap().starts_with(expected))
//         .unwrap_or_default()
// }

impl TryFrom<&conf::RemoteResponse> for RemoteResponder {
    type Error = errors::PrepareError;
    fn try_from(c: &conf::RemoteResponse) -> Result<Self, Self::Error> {
        let headers: Vec<(Vec<u8>, String)> = c
            .headers
            .as_ref()
            .map(|hd| {
                hd.iter()
                    .map(|h| h.split_once(':'))
                    .flatten()
                    .map(|(k, v)| {
                        (
                            k.to_lowercase().trim().as_bytes().into(),
                            v.trim().to_string(),
                        )
                    })
                    .collect()
            })
            .unwrap_or_default();

        let method =
            http::Method::from_bytes(c.method.to_uppercase().as_bytes()).map_err(|_| {
                errors::PrepareError::GenericError(format!("invalid status code: {}", c.method))
            })?;

        let rhai = if let Some(s) = &c.script {
            Some(
                Script::from_str(s)
                    .map_err(|err| errors::PrepareError::GenericError(err.to_string()))?,
            )
        } else {
            None
        };

        Ok(RemoteResponder {
            conf: c.clone(),
            headers,
            method,
            rhai,
        })
    }
}

#[async_trait(?Send)]
impl Responder for RemoteResponder {
    async fn respond(
        &self,
        resp: &mut HttpResponseBuilder,
        ctx: &ConnectionContext,
    ) -> Result<HttpResponse, errors::ResponseError> {
        let remote = &self.conf;
        debug!("remote call {:?}", &remote);

        let config = self.create_client_config()?;
        let client = actix_web::client::ClientBuilder::new()
            .connector(Connector::new().rustls(Arc::new(config)).finish())
            .finish();
        let mut client = client.request(self.method.clone(), &remote.url);

        if remote.copy_request_headers {
            let hm = client.headers_mut();
            ctx.headers.iter().for_each(|(k, v)| {
                hm.insert(
                    http::HeaderName::from_lowercase(k.to_lowercase().as_bytes()).unwrap(),
                    http::HeaderValue::from_str(v).unwrap(),
                );
            });
        }

        if !self.headers.is_empty() {
            let hm = client.headers_mut();
            self.headers.iter().for_each(|(k, v)| {
                hm.insert(
                    http::HeaderName::from_lowercase(k).unwrap(),
                    http::HeaderValue::from_str(v).unwrap(),
                );
            });
        }

        let client = if remote.send_request_body {
            client.send_body(actix_web::dev::Body::from(ctx.req_body.to_owned()))
        } else {
            client.send()
        };

        let mut response = client.await?;
        let body = response.body().await?;
        // let body = self.process_response(body, &response)?;

        if remote.copy_status {
            resp.status(response.status());
        }
        Ok(resp.body(body))
    }
}

#[derive(Debug, Clone)]
pub struct Response {
    service_name: String,
    /// response source
    responder: Arc<dyn Responder>,
    /// response http status code
    response_status: http::StatusCode,
    /// response headers
    response_headers: Option<Vec<HeaderValue>>,
}

impl TryFrom<&conf::Service> for Response {
    type Error = errors::PrepareError;

    fn try_from(service: &conf::Service) -> Result<Self, Self::Error> {
        let r = &service.response;
        let service_name = service.name.to_owned();
        let res = match r {
            conf::Response::SimpleResponse(x) => Response {
                service_name,
                responder: Arc::new(TextResponder::from_str(x)?),
                response_status: http::StatusCode::OK,
                response_headers: None,
            },
            conf::Response::DetailedResponse(dr) => {
                let status = new_status_code(dr.status)?;
                let mut h = if let Some(rp) = &dr.headers {
                    let hv = rp
                        .iter()
                        .map(HeaderValue::try_from)
                        .collect::<Result<Vec<HeaderValue>, errors::PrepareError>>()?;
                    Some(hv)
                } else {
                    None
                };

                if let Some(ct) = &dr.content_type {
                    h.get_or_insert(Vec::new())
                        .push(HeaderValue::Plain(("content-type".to_string(), ct.clone())));
                }

                let resp: Arc<dyn Responder> = match (dr.handlebars, &dr.text, &dr.file, &dr.remote)
                {
                    (true, Some(text), None, None) => Arc::new(HbResponder::try_new_text(text)?),
                    (true, None, Some(file), None) => Arc::new(HbResponder::try_new_file(file)?),
                    (false, Some(text), None, None) => Arc::new(TextResponder::from_str(text)?),
                    (false, None, Some(file), None) => Arc::new(FileResponder::from_str(file)?),
                    (_, None, None, Some(rc)) => Arc::new(RemoteResponder::try_from(rc)?),
                    (_, None, None, None) => Arc::new(EmptyResponder {}),
                    _ => panic!("invalid combination"),
                };

                Response {
                    service_name,
                    responder: resp,
                    response_status: status,
                    response_headers: h,
                }
            }
        };
        Ok(res)
    }
}

impl Response {
    pub async fn build_response<'a>(
        &self,
        ctx: ConnectionContext,
        service_name: &str,
    ) -> HttpResponse {
        // take script result status first; if not set - use configured response_status
        let response_status = ctx
            .response_status
            .and_then(|s| new_status_code(s).ok())
            .unwrap_or(self.response_status);

        let mut resp = HttpResponse::build(response_status);
        put_headers_in_resp(&ctx, &self.response_headers, &mut resp);

        // put service name in header
        resp.header("X-MOCKER-SERVICE", self.service_name.clone());

        // add headers from script result
        ctx.response_headers.iter().for_each(|(name, value)| {
            resp.header(name, value.clone());
        });

        match self.responder.respond(&mut resp, &ctx).await {
            Err(err) => {
                error!(
                    "create response for service {} error: {}",
                    service_name, err
                );
                resp.status(http::StatusCode::INTERNAL_SERVER_ERROR)
                    .body(err.to_string())
            }
            Ok(res) => res,
        }
    }
}

impl fmt::Display for Response {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.responder)?;
        write!(f, "; status: {:?}", self.response_status)?;
        write!(f, "; headers: {:?}", self.response_headers)
    }
}

fn new_status_code(status: u16) -> Result<http::StatusCode, errors::PrepareError> {
    http::StatusCode::from_u16(status).map_err(|err| {
        errors::PrepareError::GenericError(format!("invalid status code `{}`: {}", status, err))
    })
}
