//
// json.rs
// Copyright (C) 2021 Karol Będkowski <Karol Będkowski@kkomp>
// Distributed under terms of the GPLv3 license.
//

use serde_json::Value;

fn json_query_p(value: &Value, path: &[&str]) -> Option<Value> {
    trace!("value: {:?} path: {:?}", value, path);
    if path.is_empty() {
        return Some(value.clone());
    }

    let head = path[0];
    let rest = &path[1..];
    match value {
        Value::Object(map) => match map.get(head) {
            Some(v) => json_query_p(v, rest),
            None => None,
        },
        Value::Array(arr) => match head.parse::<usize>() {
            Ok(v) if v < arr.len() => json_query_p(&arr[v], rest),
            _ => None,
        },
        _ => None,
    }
}

pub fn json_query(value: &Value, path: &str) -> Option<Value> {
    if path.is_empty() {
        return None;
    }
    let path = path.split_terminator('.').collect::<Vec<&str>>();
    json_query_p(value, &path)
}

// pub fn json_query_to_str_scalars(value: &Value, path: &str) -> Option<String> {
//     let res = json_query(value, path);
//     match res {
//         Some(Value::Number(n)) => Some(n.to_string()),
//         Some(Value::String(s)) => Some(s),
//         Some(Value::Bool(b)) => Some(b.to_string()),
//         _ => None,
//     }
// }

pub fn json_query_to_str(value: &Value, path: &str) -> Option<String> {
    let res = json_query(value, path);
    match res {
        Some(Value::Number(n)) => Some(n.to_string()),
        Some(Value::String(s)) => Some(s),
        Some(Value::Bool(b)) => Some(b.to_string()),
        Some(x) => serde_json::to_string(&x).ok(),
        _ => None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::*;

    #[test]
    fn test1() {
        let testjson = json!({
            "name": "John Doe",
            "age": 43,
            "phones": [
                "+44 1234567",
                "+44 2345678"
            ],
            "data": {
                "subdata": {
                    "ss1": 12,
                    "ss2": 22,
                },
                "subdata2": {
                    "ss3": 32,
                    "ss4": 42,
                }
            }
        });

        let res = json_query(&testjson, "name.age");
        assert!(res.is_none());

        let res = json_query(&testjson, "age");
        assert_eq!(res, Some(Value::Number(Number::from(43))));

        let res = json_query(&testjson, "data.subdata");
        assert_eq!(res, Some(json!({"ss1": 12, "ss2": 22})));
        let res = json_query(&testjson, "data.subdata.ss1");
        assert_eq!(res, Some(Value::Number(Number::from(12))));
        let res = json_query(&testjson, "data.subdata.ss2");
        assert_eq!(res, Some(Value::Number(Number::from(22))));
        let res = json_query(&testjson, "data.subdata2.ss3");
        assert_eq!(res, Some(Value::Number(Number::from(32))));
        let res = json_query(&testjson, "data.subdata2.ss4");
        assert_eq!(res, Some(Value::Number(Number::from(42))));

        let res = json_query(&testjson, "phones.1");
        assert_eq!(res, Some(Value::String(String::from("+44 2345678"))));
    }

    #[test]
    fn test2() {
        let testjson = json!({
            "name": "John Doe",
            "age": 43,
            "phones": [
                "+44 1234567",
                "+44 2345678"
            ],
            "data": {
                "subdata": {
                    "ss1": 12,
                    "ss2": 22,
                },
                "subdata2": {
                    "ss3": 32,
                    "ss4": 42,
                }
            }
        });

        let res = json_query_to_str(&testjson, "name.age");
        assert!(res.is_none());

        let res = json_query_to_str(&testjson, "age");
        assert_eq!(res, Some(String::from("43")));

        let res = json_query_to_str(&testjson, "data.subdata.ss1");
        assert_eq!(res, Some(String::from("12")));
        let res = json_query_to_str(&testjson, "data.subdata.ss2");
        assert_eq!(res, Some(String::from("22")));
        let res = json_query_to_str(&testjson, "data.subdata2.ss3");
        assert_eq!(res, Some(String::from("32")));
        let res = json_query_to_str(&testjson, "data.subdata2.ss4");
        assert_eq!(res, Some(String::from("42")));

        let res = json_query_to_str(&testjson, "phones.1");
        assert_eq!(res, Some(String::from("+44 2345678")));
    }
}
