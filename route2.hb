route2.h


service name: {{ service_name }}

path: {{ path }}

method: {{ method }}

header x-aa: {{ headers.x-aa }}

query search param: {{ query.search }}

query_string: {{ query_string }}

json: {{ req_json }}

body: {{ req_body }}


globals 
=======
globals.test: {{ globals.test }}


locals
======
locals.examples: {{ locals.examples }}
locals.message: {{ locals.message }}


Connection-info
===============
ssl: {{ connection.ssl }}
peer_cert_sn: {{ connection.peer_cert_sn }}
peer_cert_issuer_cn: {{ connection.peer_cert_issuer_cn }}
peer_cert_subject_cn: {{ connection.peer_cert_subject_cn }}
peer_cert_subject_san: {{ connection.peer_cert_subject_san }}

remote_address_ip: {{ connection.remote_address_ip }}



some helpers are available
==========================

{{#to_json format="toml"}}
[foo]
bar = { baz = true }
hello = "1.2.3"
{{/to_json}}


Get json:
{{#json_to_str locals}}{{/json_to_str}}


{{ assign "j" {"aa": [1, 2 , 3]} }}

Get json value by path
aa: {{json-path j "aa" }}
aa.2: {{json-path j "aa.2" }}


RHAI helpers

2*3={{rhai_mul 2 3}}
